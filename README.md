# Deep Reinforcement Learning with OpenAI

I will implement a Deep Reinforcement Learning algorithm on a classic control task in the OpenAI AI-Gym Environment. Specifically, I will implement Q-Learning using a Neural Network as an approximator for the Q-function.

**Note Tensorflow version should be <2.0**
jupyter-notebook --no-browser --port=5000
pip3 install tensorflow=1.9
pip3 install gym