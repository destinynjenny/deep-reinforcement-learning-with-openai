import gym
import tensorflow as tf
import numpy as np
import random

# General Parameters
# -- DO NOT MODIFY --
ENV_NAME = 'CartPole-v0'
EPISODE = 200000  # Episode limitation
STEP = 200  # Step limitation in an episode
TEST = 10  # The number of tests to run every TEST_FREQUENCY episodes
TEST_FREQUENCY = 100  # Num episodes to run before visualizing test accuracy

# TODO: HyperParameters
GAMMA = 0.9  # discount factor
INITIAL_EPSILON = 0.6  # starting value of epsilon
FINAL_EPSILON = 0.22  # final value of epsilon
EPSILON_DECAY_STEPS = 100  # decay period
BATCH_SIZE = 500  # agent memory size
HIDDEN_NODES = 60
MEMORY = 15000

global Agent_brain_memory  # this is experience memory list
Agent_brain_memory = []
# if flag equals true, it means my model close to the limitation, to prevent overfit, I will stop train my DQN
global flag
flag = False


# Create environment
# -- DO NOT MODIFY --
env = gym.make(ENV_NAME)
epsilon = INITIAL_EPSILON
STATE_DIM = env.observation_space.shape[0]
ACTION_DIM = env.action_space.n

# Placeholders
# -- DO NOT MODIFY --
state_in = tf.placeholder("float", [None, STATE_DIM])
action_in = tf.placeholder("float", [None, ACTION_DIM])
target_in = tf.placeholder("float", [None])


# TODO: Define Network Graph
def graph_nn(state_in, state_dim, hidden_nodes, action_dim):
    W1 = tf.get_variable("W1", shape=[state_dim, hidden_nodes], initializer=tf.random_normal_initializer(mean=0.1, stddev=.1) )
    b1 = tf.get_variable("b1", shape=[1, hidden_nodes], initializer=tf.constant_initializer(0.1))

    W2 = tf.get_variable("W2", shape=[hidden_nodes, action_dim], initializer=tf.random_normal_initializer(mean=0.1, stddev=.1) )
    b2 = tf.get_variable("b2", shape=[1, action_dim], initializer=tf.constant_initializer(0.1))

    # This is a dueling network, which will make the training result more stable
    # Additional Layer Advantage
    Wa = tf.get_variable("Wa", shape=[hidden_nodes, action_dim],                   initializer=tf.random_normal_initializer(mean=0.1, stddev=.1))
    Ba = tf.get_variable("ba", shape=[1, action_dim], initializer=tf.constant_initializer(0.1))

    # Layer1
    logits_layer1 = tf.matmul(state_in, W1) + b1
    output_layer1 = tf.nn.relu(logits_layer1)  # tf.sigmoid(logits_layer1) or tf.tanh(logits_layer1) or ... ?

    # Layer2
    logits_layer2 = tf.matmul(output_layer1, W2) + b2

    # Layer Advantage: calculate the action advantage
    advantage = tf.matmul(output_layer1, Wa) + Ba

    # q value = q value from layer 2 + action advantage
    out = logits_layer2 + (logits_layer2 - tf.reduce_mean(advantage, axis=1, keep_dims=True))

    return out


def update_memory(Agent_brain_memory, state, action, reward, next_state, done,):
    """
    # saving the experience in memory by this function. Each memory has these features:
    # state, one_hot_action, reward, next_state, done
    """
    one_hot_action = action
    Agent_brain_memory.append((state, one_hot_action, reward, next_state, done))
    if len(Agent_brain_memory) > MEMORY:
        Agent_brain_memory.pop(0)
    return None


def get_train_batch(q_values, state_in, minibatch):
    """
    :param q_values: q value or we could say it "action" value from DQN
    :param state_in:  state
    :param minibatch: randomly chosen experience
    :return:
    """
    state_batch1 = [data[0] for data in minibatch]
    action_batch1 = [data[1] for data in minibatch]
    reward_batch = [data[2] for data in minibatch]
    next_state_batch = [data[3] for data in minibatch]
    target_batch1 = []
    Q_value_batch = q_values.eval(feed_dict={
        state_in: next_state_batch
    })

    global GAMMA

    for i in range(0, len(minibatch)):
        sample_is_done = minibatch[i][4]
        if sample_is_done:
            target_batch1.append(reward_batch[i])
        else:
            # set the target_val is used for  Q value update
            target_val = reward_batch[i] + GAMMA * np.max(Q_value_batch[i])
            target_batch1.append(target_val)
    return np.array(target_batch1), np.array(state_batch1), np.array(action_batch1)


# TODO: Network outputs
q_values = graph_nn(state_in, STATE_DIM, HIDDEN_NODES, ACTION_DIM)
q_action = tf.reduce_sum(tf.multiply(q_values, action_in), reduction_indices=1)

# TODO: Loss/Optimizer Definition
# loss = tf.reduce_mean(tf.squared_difference(target_in,q_action))
loss = tf.reduce_sum(tf.square(target_in - q_action))
optimizer = tf.train.AdamOptimizer(0.001).minimize(loss=loss)

# Start session - Tensorflow housekeeping
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())


# -- DO NOT MODIFY ---
def explore(state, epsilon):
    """
    Exploration function: given a state and an epsilon value,
    and assuming the network has already been defined, decide which action to
    take using e-greedy exploration based on the current q-value estimates.
    """
    Q_estimates = q_values.eval(feed_dict={
        state_in: [state]
    })
    if random.random() <= epsilon:
        action = random.randint(0, ACTION_DIM - 1)
    else:
        action = np.argmax(Q_estimates)
    one_hot_action = np.zeros(ACTION_DIM)
    one_hot_action[action] = 1
    return one_hot_action


# Main learning loop

for episode in range(EPISODE):

    # initialize task
    state = env.reset()

    # Update epsilon once per episode
    epsilon -= epsilon / EPSILON_DECAY_STEPS
    if epsilon < FINAL_EPSILON:
        flag = True

    # Move through env according to e-greedy policy
    for step in range(STEP):
        action = explore(state, epsilon)
        next_state, reward, done, _ = env.step(np.argmax(action))

        # nextstate_q_values = q_values.eval(feed_dict={
        #     state_in: [next_state]
        # })
        # print(nextstate_q_values)

        # TODO: Calculate the target q-value.
        # hint1: Bellman
        # hint2: consider if the episode has terminated
        target = 0
        update_memory(Agent_brain_memory, state, action, reward, next_state, done)

        if len(Agent_brain_memory) >= BATCH_SIZE and not flag:
            minibatch = random.sample(Agent_brain_memory, BATCH_SIZE)
            target, state, action = get_train_batch(q_values, state_in, minibatch)
        # print(tf.shape(state))
        # print(len(Agent_brain_memory))
        # Do one training step
            session.run([optimizer], feed_dict={
                target_in: target,
                action_in: action,
                state_in: state
            })
        elif not flag:
            minibatch = Agent_brain_memory
            target, state, action = get_train_batch(q_values, state_in, minibatch)
            session.run([optimizer], feed_dict={
                target_in: target,
                action_in: action,
                state_in: state
            })

        # Update
        state = next_state
        if done:
            print("episode:{}".format(episode))
            print("step:{}".format(step))
            print("epsilon:{}".format(epsilon))
            break

    # Test and view sample runs - can disable render to save time
    # -- DO NOT MODIFY --
    if (episode % TEST_FREQUENCY == 0 and episode != 0):
        total_reward = 0
        for i in range(TEST):
            state = env.reset()
            for j in range(STEP):
                env.render()
                action = np.argmax(q_values.eval(feed_dict={
                    state_in: [state]
                }))
                state, reward, done, _ = env.step(action)
                total_reward += reward
                if done:
                    break
        ave_reward = total_reward / TEST
        print('episode:', episode, 'epsilon:', epsilon, 'Evaluation '
                                                        'Average Reward:', ave_reward)
env.close()
