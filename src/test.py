import gym
import tensorflow as tf
import numpy as np
import random
import time
# General Parameters
# -- DO NOT MODIFY --
ENV_NAME = 'CartPole-v0'
EPISODE = 200000  # Episode limitation
STEP = 200  # Step limitation in an episode
TEST = 10  # The number of tests to run every TEST_FREQUENCY episodes
TEST_FREQUENCY = 100  # Num episodes to run before visualizing test accuracy

# TODO: HyperParameters
GAMMA = 0.9 # discount factor
INITIAL_EPSILON = 0.6 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon
EPSILON_DECAY_STEPS = 100# decay period
NODES = 60

on_off = True

# Create environment
# -- DO NOT MODIFY --
env = gym.make(ENV_NAME)
epsilon = INITIAL_EPSILON
STATE_DIM = env.observation_space.shape[0]
ACTION_DIM = env.action_space.n

# Placeholders
# -- DO NOT MODIFY --
state_in = tf.placeholder("float", [None, STATE_DIM])
action_in = tf.placeholder("float", [None, ACTION_DIM])
target_in = tf.placeholder("float", [None])

# TODO: Define Network Graph
def dqn(state_in, state_dim,hidden_nodes, action_dim):

    W1 = tf.get_variable("W1", shape=[state_dim, hidden_nodes], )
    b1 = tf.get_variable("b1", shape=[1, hidden_nodes], initializer=tf.constant_initializer(0.1))

    # Define W and b of the second layer
    W2 = tf.get_variable("W2", shape=[hidden_nodes, hidden_nodes], )
    b2 = tf.get_variable("b2", shape=[1, hidden_nodes], initializer=tf.constant_initializer(0.1))

    # Define W and b of the third layer
    W3 = tf.get_variable("W3", shape=[hidden_nodes, action_dim])
    b3 = tf.get_variable("b3", shape=[1, action_dim], initializer=tf.constant_initializer(0.1))

    # Layer1
    logits_layer1 = tf.matmul(state_in, W1) + b1
    output_layer1 = tf.tanh(logits_layer1)

    # Layer2
    logits_layer2 = tf.matmul(output_layer1, W2) + b2
    output_layer2 = tf.tanh(logits_layer2)

    # Layer3
    logits_layer3 = tf.matmul(output_layer2, W3) + b3

    return logits_layer3

# TODO: Network outputs
#calculate the value of each action base on the state [value of a0, value of a1]
q_values = dqn(state_in, STATE_DIM, NODES, ACTION_DIM)

q_action = tf.reduce_sum(tf.multiply(action_in,q_values),axis=1)

# TODO: Loss/Optimizer Definition
loss = tf.reduce_mean(tf.squared_difference(target_in,q_action))
optimizer = tf.train.AdamOptimizer(0.01,name="optimizer").minimize(loss=loss)

# Start session - Tensorflow housekeeping
session = tf.InteractiveSession()
session.run(tf.global_variables_initializer())

# TODO: experience
MEMORY_SIZE = 10000
MEMORY_BATCH_SIZE = 300
global Brain
Brain = []

def experience(state, action, reward, next_state, done,):
    Brain.append((state, action, reward, next_state, done))
    if len(Brain) > MEMORY_SIZE:
        Brain.pop(0)

def train(q_values, state_in, testbatch):
    state_batch=[]
    action_batch = []
    reward_batch = []
    next_state_batch = []
    target_q = []

    for i in testbatch:
        state_batch.append(i[0])
        action_batch.append(i[1])
        reward_batch.append(i[2])
        next_state_batch.append(i[3])

    Q_future = q_values.eval(feed_dict={
        state_in: next_state_batch
    })

    global GAMMA

    for i in range(0, len(testbatch)):
        sample_is_done = testbatch[i][4]
        if sample_is_done:
            target_q.append(reward_batch[i])
        else:
            # set the target_val is used for  Q value update
            target_val = reward_batch[i] + GAMMA * np.max(Q_future[i])
            target_q.append(target_val)
    return np.array(target_q), np.array(action_batch), np.array(state_batch)


# -- DO NOT MODIFY ---
def explore(state, epsilon):
    """
    Exploration function: given a state and an epsilon value,
    and assuming the network has already been defined, decide which action to
    take using e-greedy exploration based on the current q-value estimates.
    """
    Q_estimates = q_values.eval(feed_dict={
        state_in: [state]
    })
    if random.random() <= epsilon:
        action = random.randint(0, ACTION_DIM - 1)
    else:
        action = np.argmax(Q_estimates)
    one_hot_action = np.zeros(ACTION_DIM)
    one_hot_action[action] = 1
    return one_hot_action


# Main learning loop
for episode in range(EPISODE):

    # initialize task
    state = env.reset()

    # Update epsilon once per episode

    epsilon -= epsilon / EPSILON_DECAY_STEPS
    if epsilon < FINAL_EPSILON:
        epsilon = FINAL_EPSILON
    # Move through env according to e-greedy policy
    for step in range(STEP):
        # env.render()
        action = explore(state, epsilon)
        next_state, reward, done, _ = env.step(np.argmax(action))
        experience(state, action, reward, next_state, done)

        testbatch = []

        if on_off:
            if len(Brain) >= MEMORY_BATCH_SIZE:
                testbatch = random.sample(Brain, MEMORY_BATCH_SIZE)

            else:
                testbatch = Brain

            target, action, state = train(q_values, state_in, testbatch)
            session.run([optimizer], feed_dict={
                target_in: target,
                action_in: action,
                state_in: state
            })

        # Update
        state = next_state

        if done:
            if step >120:
                on_off = False

            # print("episode:{}".format(episode))
            # print("step:{}".format(step))
            # print("epsilon:{}".format(epsilon))
            break
    # Test and view sample runs - can disable render to save time
    # -- DO NOT MODIFY --
    if (episode % TEST_FREQUENCY == 0 and episode != 0):
        total_reward = 0
        for i in range(TEST):
            state = env.reset()
            for j in range(STEP):
                # env.render()
                action = np.argmax(q_values.eval(feed_dict={
                    state_in: [state]
                }))
                state, reward, done, _ = env.step(action)
                total_reward += reward
                if done:
                    break
        ave_reward = total_reward / TEST
        print('episode:', episode, 'epsilon:', epsilon, 'Evaluation '
                                                        'Average Reward:', ave_reward)

env.close()
