# import gym
# env = gym.make('CartPole-v0')
# env.reset()
# for _ in range(100):
#     env.render()
#     action = env.action_space.sample()
#     observation=env.step(env.action_space.sample())
#     print(observation)
#     print(env.observation_space.shape[0])
#     print(action)
#     print(env.action_space.n)
#
# env.close()
import tensorflow as tf

# x => shape[2,4]
x = tf.constant([[1.,1.,1.,1.],[2.,2.,2.,2.]])
# y => shape [4,2]
y = tf.constant([[1.,1.],[2.,2.],[3.,3.],[4.,4.]])

b = tf.constant([[1.,1.],[1.,1.]])

z = tf.matmul(x, y)+b


x2 = tf.constant([[1.,1.],[2.,2.]])
y2 = tf.constant([[2.,2.],[4.,1.]])
z2 = tf.multiply(x2,y2)


with tf.Session() as sess:
    print("#####matmul######")
    print(sess.run(z))
    print(sess.run(tf.sigmoid(z)))
    print("#####multiply######")
    print(sess.run(z2))
    print(sess.run(tf.reduce_sum(z2)))
